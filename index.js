// Hello World
console.log("Hello World!");

// Prompt the user for two numbers
var a = parseInt(prompt("Enter first number:"));
var b = parseInt(prompt("Enter second number:"));
var getSum = a + b;
var getDiff = a - b;
var getProd = a * b;
var getQuotient = a/b;

// If else
if (getSum < 10) {
	console.warn("The sum is " + getSum);
} else if (getSum >= 10 && getSum <=20) {
	alert("The difference is " + getDiff);
} else if (getSum > 20 && getSum <=30) {
	alert("The product is " + getProd);
} else if (getSum > 31) {
	alert("The quotient is " + getQuotient);
}

// Prompt the user for their name and age
var getName = prompt("Enter your name:");
var getAge = parseInt(prompt("Enter your age:"));

if (getName == "" && getAge == "") {
	console.log("Are you a time traveler?");
} else if (getName != "" && getAge != "") {
	console.log("Your name is " + getName + ", and you are " + getAge + " years old.");
}

// Create a function named isLegalAge
function isLegalAge() {
	if (getAge >= 18) {
		window.alert("You are of legal age.");
	} else {
		window.alert("You are not allowed here.");
	}
}

isLegalAge();

// Create a switch case statement
switch (getAge) {
	case getAge = 18:
		window.alert("You are not allowed to party.");
		break;
	case getAge = 21:
		window.alert("You are now part of the adult society.");
		break;
	case getAge = 65:
		window.alert("We thank you for your contribution to society.");
		break;
	default:
		window.alert("Are you sure you're not an alien.");
}

// Create a try catch finally statement
try {
	// statements
	consoled.warn(isLegalAge(getAge));
} catch(error) {
	// statements
	console.warn(error.message);
} finally {
	window.alert(isLegalAge(getAge));
}